﻿using System;
using File_Manager.Service;
using File_Manager.Prompts;

namespace File_Manager
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileService = new FileService();
            var prompts = new Prompt(fileService);

            string option;

            do
            {
                prompts.PromptStart();
                option = Console.ReadLine().ToLower();

                prompts.ReadStartPrompt(option);

            } while (option != "exit");
        }
    }
}
