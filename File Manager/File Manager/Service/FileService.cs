﻿using File_Manager.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace File_Manager.Service
{
    class FileService
    {
        private FileInfo[] fileInfos;
        private DirectoryInfo dirInfo;
        public FileService()
        {
            var dirPath = Path.GetFullPath(@"..\..\..\resources");
            dirInfo = new DirectoryInfo(dirPath);
            fileInfos = dirInfo.GetFiles();
        }

        public FileInfo GetTextFilePath()
        {
            return dirInfo.GetFiles("dracula.txt").FirstOrDefault();
        }

        public string GetTextFileName()
        {
            return GetTextFilePath().Name;
        }

        public string GetTextFileSize()
        {
            return GetTextFilePath().Length.ToString();
        }

        public int CountLinesOfTxt()
        {
            return File.ReadLines(GetTextFilePath().FullName).Count();
        }

        public int CountWords(string word)
        {
            var txt = File.ReadAllText(GetTextFilePath().ToString());
            return txt.ToLower().Split(word.ToLower()).Length - 1;
        }

        public List<string> GetExtensions()
        {
            var fileExtensions = new List<string>();
            foreach (var file in fileInfos)
            {
                var fileExtension = file.Extension.Trim('.');
                if (!fileExtensions.Contains(fileExtension))
                    fileExtensions.Add(fileExtension);
            }
            return fileExtensions;
        }
        public FileInfo[] ShowFilesByExtensions(string extension)
        {
            return dirInfo.GetFiles("*." + extension);
        }

        public IOrderedEnumerable<FileInfo> GetFiles()
        {
            return fileInfos.OrderBy(file => file.Name, new AlphanumComparator());
        }
    }
}
