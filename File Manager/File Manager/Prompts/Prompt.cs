﻿using File_Manager.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace File_Manager.Prompts
{
    class Prompt
    {
        private FileService fileService;
        public Prompt(FileService fileService)
        {
            this.fileService = fileService;
        }
        public void PromptStart()
        {
            Console.WriteLine("\nWelcome to file system manager console!\n");
            Console.WriteLine("1:\t Show all files in directory");
            Console.WriteLine("2:\t Show files by extension");
            Console.WriteLine("3:\t Manipulate Dracula.txt");
            Console.WriteLine("Exit:\t Close application");
            Console.Write("Choose an option: ");      
        }
        private void ShowFiles()
        {
            var files = fileService.GetFiles();
            foreach (var file in files)
            {
                Console.WriteLine(file.Name);
            }
        }

        public Dictionary<string, string> GetExtensionDictornary()
        {
            var dictonary = fileService.GetExtensions()
                .Select((item, index) => new { index, item })
                .ToDictionary(item => (item.index + 1).ToString(), item => item.item);

            dictonary.Add("Back", "Go back to menu");

            return dictonary;
        }
        private void ShowFileExtensions(Dictionary<string, string> options)
        {
            foreach (var item in options)
            {
                Console.WriteLine(item.Key + ": " + item.Value);
            }
        }
        private void ShowFileByExtensions(Dictionary<string, string> options, string key)
        {
            if (options.ContainsKey(key))
            {
                var fileExtensions = fileService.ShowFilesByExtensions(options[key]);

                foreach (var file in fileExtensions)
                {
                    Console.WriteLine(file.Name);
                }
            }
            else
                PromptInvalidCommand();
                
        }
        public void PromptTextInfo()
        {
            Console.WriteLine($"Show info of {fileService.GetTextFileName()}");
            Console.WriteLine("1:\t Show size of file");
            Console.WriteLine("2:\t Check how many lines the text file has");
            Console.WriteLine("3:\t Check how many times a specific word occurs in text file");
            Console.WriteLine("Back:\t Go Back to menu");

            Console.Write("Choose an option: ");
        }
        public void PromptCountWords(string word)
        {
            var wordCount = fileService.CountWords(word);

            if (wordCount <= 0)
                Console.WriteLine($"The word: {word} does not exist");

            Console.WriteLine($"{word} occurs {wordCount} times");
        }
        public void PromptTextFileSize()
        {
            Console.WriteLine("File size: " + fileService.GetTextFileSize());
        }
        public void PromptTextFileLines()
        {
            Console.WriteLine("Lines in text file: " + fileService.CountLinesOfTxt());
        }

        private void LineSeperator()
        {
            Console.WriteLine("*********************************************************************\n");
        }
        public void PromptTextInfoOption(string option)
        {
            var optionNumber = option.ToLower();
            switch (optionNumber)
            {
                case "1":
                    PromptTextFileSize();
                    break;
                case "2":
                    PromptTextFileLines();
                    break;
                case "3":
                    Console.Write("Write a word to search: ");
                    var word = Console.ReadLine();
                    PromptCountWords(word);
                    break;
                case "back":
                    return;
                default:
                    PromptInvalidCommand();
                    break;
            }
        }
        public void ReadStartPrompt(string option)
        {
            var optionNumber = option.ToLower();
            switch (optionNumber)
            {
                case "exit":
                    Environment.Exit(0);
                    break;
                case "1":
                    Console.WriteLine("Showing all files:");
                    ShowFiles();
                    break;
                case "2":
                    PromptExtensionMenu();
                    break;
                case "3":
                    runTextInfoPrompt();
                    break;
                default:
                    PromptInvalidCommand();
                    break;
            }
        }

        public void PromptExtensionMenu()
        {
            string extension;
            do
            {
                LineSeperator();
                Console.WriteLine("Extension in the directory:");
                var options = GetExtensionDictornary();
                ShowFileExtensions(options);
                Console.Write("Select an option: ");
                extension = Console.ReadLine();
                ShowFileByExtensions(options, extension);
            } while (extension != "back");
            
        }

        public void PromptInvalidCommand()
        {
            Console.WriteLine("Command was invalid, try again");
        }

        public void runTextInfoPrompt()
        {
            string textInfoOption;
            do
            {
                LineSeperator();
                PromptTextInfo();
                textInfoOption = Console.ReadLine().ToLower();
                PromptTextInfoOption(textInfoOption);
            } while (textInfoOption != "back");
        }
    }
}
